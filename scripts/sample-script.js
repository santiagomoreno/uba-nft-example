const { ethers } = require("hardhat");

async function main() {
  const UBACollection = await ethers.getContractFactory("UBACollection");
  const ubaCollection = await UBACollection.deploy("Universidad de Buenos Aires", "UBA");

  await ubaCollection.deployed();

  console.log("UBA Collection deployed to:", ubaCollection.address);

  await ubaCollection.mint("https://ipfs.io/ipfs/Qme4aGMJktrH3kNND8Rshr5k3aGxkdVjZ534h2bQn5YAcu");

  console.log("NFTs successfully minted");

}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
